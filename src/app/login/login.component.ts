import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  isToLong: boolean = false;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    setTimeout(() => { this.isToLong = true }, 4000);
  }

  ngAfterViewInit() {
    this.auth.login();
  }
}
