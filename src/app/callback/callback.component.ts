import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-callback',
  templateUrl: '../login/login.component.html',
  styleUrls: ['../login/login.component.scss']
})
export class CallbackComponent implements OnInit {

  isToLong: boolean = false;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {

    setTimeout(() => { this.isToLong = true }, 4000);

    if (this.auth.isAuthenticated) {
      console.log('User successfully registered');
    } else {
      this.router.navigate(['/']);
    }
  }

}
