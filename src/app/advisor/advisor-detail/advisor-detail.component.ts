import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Advisor } from 'src/app/models/advisor';
import { Observable } from 'rxjs';
import { AdvisorService } from 'src/app/services/advisor/advisor.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: '[app-advisor-detail]',
  templateUrl: './advisor-detail.component.html',
  styleUrls: ['./advisor-detail.component.scss']
})
export class AdvisorDetailComponent implements OnInit {

  @Input() advisor: Observable<Advisor> = null;
  @Output() updatedAdvisorList = new EventEmitter<Observable<Advisor[]>>();
  @Output() onEditEvent = new EventEmitter<{state: boolean, event: any}>();

  onEdit: boolean = false;

  constructor(private advisorService: AdvisorService, private fb: FormBuilder) { }

  ngOnInit() {
  }

  deleteAdvisor(idAdvisor) {
    this.advisorService.delete(idAdvisor)
      .subscribe(data => {
        this.updatedAdvisorList.emit(this.advisorService.list());
      });
  }

  toggleEdit(advisor: Advisor) {
    this.onEdit = !this.onEdit;
    this.onEditEvent.emit({ state: this.onEdit, event: advisor });
  }
}
