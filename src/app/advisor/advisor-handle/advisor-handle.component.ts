import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Advisor } from 'src/app/models/advisor';
import { AdvisorService } from 'src/app/services/advisor/advisor.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-advisor-handle',
  templateUrl: './advisor-handle.component.html',
  styleUrls: ['./advisor-handle.component.scss']
})
export class AdvisorHandleComponent implements OnInit {

  advisors: Observable<Advisor[]> = null;
  showForm: boolean = false;
  onEdit: boolean = false;
  onEditAdvisorId: string = "";
  formTitle: string = "New advisor";
  form: FormGroup = this.fb.group({
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    description: ['', [Validators.required]]
  });

  constructor(private advisorService: AdvisorService, private fb: FormBuilder) { }

  ngOnInit() {
    this.advisors = this.advisorService.list();
  }

  toggleForm() {
    this.formTitle = "New advisor";
    this.form.reset();
    this.showForm = !this.showForm;
  }

  addAdvisor() {
    const newAdvisor: Advisor = Object.assign(new Advisor(), this.form.value);
    this.advisorService.add(newAdvisor)
      .subscribe(data => {
        this.advisors = this.advisorService.list();
        this.showForm = !this.showForm;
        this.onEdit = !this.onEdit;
      });
  }

  editAdvisor() {
    const updatedAdvisor: Advisor = Object.assign(new Advisor(), this.form.value);
    updatedAdvisor._id = this.onEditAdvisorId;
    console.log(updatedAdvisor._id);
    this.advisorService.update(updatedAdvisor)
      .subscribe(data => {
        this.advisors = this.advisorService.list();
        this.showForm = !this.showForm;
        this.onEdit = !this.onEdit;
      });
  }

  updatedAdvisorListToHandle(newListToHandle: Observable<Advisor[]>) {
    this.advisors = newListToHandle;
  }

  onEditEventToHandle(editEvent: {state: boolean, event: Advisor}) {
    this.onEditAdvisorId = editEvent.event._id;
    this.form.patchValue(editEvent.event);
    this.formTitle = "Edit advisor";
    this.showForm = editEvent.state;
    this.onEdit = editEvent.state;
  }
}
