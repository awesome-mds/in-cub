import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Advisor } from 'src/app/models/advisor';

@Component({
  selector: 'app-advisor-list',
  templateUrl: './advisor-list.component.html',
  styleUrls: ['./advisor-list.component.scss']
})
export class AdvisorListComponent implements OnInit {

  @Input() advisors: Observable<Advisor[]> = null;
  @Output() updatedAdvisorListToHandle = new EventEmitter<Observable<Advisor[]>>();
  @Output() onEditEventToHandle = new EventEmitter<{state: boolean, event: any}>();

  tableColumns: string[] = ['Last name', 'First name', 'Description', 'Actions'];

  constructor() { }

  ngOnInit() {
  }

  onEditEvent(editEvent: { state: boolean, event: Advisor }) {
    this.onEditEventToHandle.emit(editEvent);
  }

  updatedAdvisorList(newList: Observable<Advisor[]>){
    this.updatedAdvisorListToHandle.emit(newList);
  }
}
