import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { environment } from 'src/environments/environment';

import { AdvisorDetailComponent } from './advisor/advisor-detail/advisor-detail.component';
import { AdvisorHandleComponent } from './advisor/advisor-handle/advisor-handle.component';
import { AdvisorListComponent } from './advisor/advisor-list/advisor-list.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CallbackComponent } from './callback/callback.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CoFoundersNumberPipe } from './pipes/co-founders-number.pipe';
import { IsAnAddressPipe } from './pipes/is-an-address.pipe';
import { AuthService } from './services/auth/auth.service';
import { InMemoryDataService } from './services/in-memory-data.service';
import { StartupDetailComponent } from './startup/startup-detail/startup-detail.component';
import { StartupHandleComponent } from './startup/startup-handle/startup-handle.component';
import { StartupListComponent } from './startup/startup-list/startup-list.component';
import { TokenInterceptor } from './services/auth/token-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    StartupListComponent,
    StartupDetailComponent,
    StartupHandleComponent,
    IsAnAddressPipe,
    CoFoundersNumberPipe,
    AdvisorDetailComponent,
    AdvisorHandleComponent,
    AdvisorListComponent,
    NotFoundComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    CallbackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // FAKE API MODULE
    // environment.production ? [] : HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { delay: 500 }),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatInputModule
  ],
  providers: [
    AuthService,
    // COMMENT ME IF YOU WANT USE FAKE API
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
    // END COMMENT ME
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
