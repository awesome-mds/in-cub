import { Advisor } from "./advisor";

export class StartUp {
  _id: string;
  name: string;
  businessLine: string;
  legalRepresentative: string;
  coFoundersNumber: number;
  description: string;
  address: string;
  advisor: Advisor;

  constructor(
    id: string = "",
    name: string = "",
    businessLine: string = "",
    legalRepresentative: string = "",
    coFoundersNumber: number = 0,
    description: string = "",
    address: string = "",
    advisor: Advisor = null
  ) {
    this._id = id;
    this.name = name;
    this.businessLine = businessLine;
    this.legalRepresentative = legalRepresentative;
    this.coFoundersNumber = coFoundersNumber;
    this.description = description;
    this.address = address;
    this.advisor = advisor;
  }
}
