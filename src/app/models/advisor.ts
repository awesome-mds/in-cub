export class Advisor {
  _id: string;
  lastName: string;
  firstName: string;
  description: string;

  constructor(
    id: string = "",
    lastName: string = "",
    firstName: string = "",
    description: string = ""
  ) {
    this._id = id;
    this.lastName = lastName;
    this.firstName = firstName;
    this.description = description;
  }
}
