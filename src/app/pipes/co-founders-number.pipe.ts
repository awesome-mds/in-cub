import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coFoundersNumber'
})
export class CoFoundersNumberPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    return (value === 1) ? "unique" : ((value === 2) ? "couple" : "group");
  }

}
