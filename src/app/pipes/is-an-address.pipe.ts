import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isAnAddress'
})
export class IsAnAddressPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return (value != null && value !== '') ? "yes" : "no";
  }

}
