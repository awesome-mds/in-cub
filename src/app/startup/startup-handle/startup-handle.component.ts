import { Component, OnInit } from '@angular/core';
import { StartUp } from 'src/app/models/start-up';
import { Observable } from 'rxjs';
import { StartupService } from 'src/app/services/startup/startup.service';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Advisor } from 'src/app/models/advisor';
import { AdvisorService } from 'src/app/services/advisor/advisor.service';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-startup-handle',
  templateUrl: './startup-handle.component.html',
  styleUrls: ['./startup-handle.component.scss']
})
export class StartupHandleComponent implements OnInit {

  startups: Observable<StartUp[]> = null;
  advisors: Observable<Advisor[]> = null;
  showForm: boolean = false;
  onEdit: boolean = false;
  onEditStartupId: string = "";
  formTitle: string = "New startup";
  form: FormGroup = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(20)]],
    businessLine: ['', [Validators.required, Validators.maxLength(10)]],
    legalRepresentative: ['', [Validators.required, Validators.maxLength(15)]],
    coFoundersNumber: ['', [Validators.required]],
    description: ['', [Validators.required, Validators.maxLength(250)]],
    address: ['', [Validators.maxLength(25)]],
    advisor: ['', [Validators.required]]
  });

  constructor(private startupService: StartupService, private advisorService: AdvisorService, private fb: FormBuilder) { }

  ngOnInit() {
    this.startups = this.startupService.list();
    this.advisors = this.advisorService.list();
  }

  toggleForm() {
    this.formTitle = "New startup";
    this.form.reset();
    this.showForm = !this.showForm;
  }

  addStartUp() {
    const newStartUp: StartUp = Object.assign(new StartUp(), this.form.value);
    this.startupService.add(newStartUp)
      .subscribe(data => {
        this.startups = this.startupService.list();
        this.showForm = !this.showForm;
        this.onEdit = !this.onEdit;
      });
  }

  editStartUp(id: string) {
    const updatedStartup: StartUp = Object.assign(new StartUp(), this.form.value);
    updatedStartup._id = this.onEditStartupId
    this.startupService.update(updatedStartup)
      .subscribe(data => {
        this.startups = this.startupService.list();
        this.showForm = !this.showForm;
        this.onEdit = !this.onEdit;
      })
  }

  updatedStartupListToHandle(newListToHandle: Observable<StartUp[]>) {
    this.startups = newListToHandle;
  }

  onEditEventToHandle(editEvent: { state: boolean, event: StartUp }) {
    this.onEditStartupId = editEvent.event._id;
    this.form.patchValue(editEvent.event);
    this.formTitle = "Edit startup";
    this.showForm = editEvent.state;
    this.onEdit = editEvent.state;
  }
}
