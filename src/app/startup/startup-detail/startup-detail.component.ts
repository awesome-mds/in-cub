import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StartUp } from 'src/app/models/start-up';
import { Observable } from 'rxjs';
import { StartupService } from 'src/app/services/startup/startup.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AdvisorService } from 'src/app/services/advisor/advisor.service';
import { Advisor } from 'src/app/models/advisor';

@Component({
  selector: '[app-startup-detail]',
  templateUrl: './startup-detail.component.html',
  styleUrls: ['./startup-detail.component.scss']
})
export class StartupDetailComponent implements OnInit {

  @Input() startup: Observable<StartUp> = null;
  @Output() updatedStartupList = new EventEmitter<Observable<StartUp[]>>();
  @Output() onEditEvent = new EventEmitter<{state: boolean, event: any}>();
  
  advisors: Observable<Advisor[]> = null;
  onEdit: boolean = false;

  constructor(private startupService: StartupService, private advisorService: AdvisorService, private fb: FormBuilder) { }

  ngOnInit() {
    this.advisors = this.advisorService.list();
  }

  toggleEdit(startup: StartUp) {
    this.onEdit = !this.onEdit;
    this.onEditEvent.emit({ state: this.onEdit, event: startup });
  }

  deleteStartup(idStartUp) {
    this.startupService.delete(idStartUp)
      .subscribe(data => {
        this.updatedStartupList.emit(this.startupService.list());
      });
  }
}