import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { StartUp } from 'src/app/models/start-up';

@Component({
  selector: 'app-startup-list',
  templateUrl: './startup-list.component.html',
  styleUrls: ['./startup-list.component.scss']
})
export class StartupListComponent implements OnInit {

  @Input() startups: Observable<StartUp[]> = null;
  @Output() updatedStartupListToHandle = new EventEmitter<Observable<StartUp[]>>();
  @Output() onEditEventToHandle = new EventEmitter<{state: boolean, event: any}>();

  tableColumns: string[] = ['name', 'legal representative', 'co-founders number', 'description', 'address', 'advisor', 'actions'];

  constructor() { }

  ngOnInit() {
  }

  onEditEvent(editEvent : { state: boolean, event: StartUp }) {
    this.onEditEventToHandle.emit(editEvent);
  }

  updatedStartupList(newList: Observable<StartUp[]>){
    this.updatedStartupListToHandle.emit(newList);
  }

}
