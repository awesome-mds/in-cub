import { Component, OnInit } from '@angular/core';
import { StartupService } from '../services/startup/startup.service';
import { AdvisorService } from '../services/advisor/advisor.service';
import { Observable } from 'rxjs';
import { Advisor } from '../models/advisor';
import { StartUp } from '../models/start-up';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  advisors: Observable<Advisor[]> = null;
  startups: Observable<StartUp[]> = null;
  profile: any;

  constructor(private startupService: StartupService, private advisorService: AdvisorService, public auth: AuthService) { }

  ngOnInit() {
    this.advisors = this.advisorService.list();
    this.startups = this.startupService.list();

    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
      console.log(this.profile);
    } else {
      // this.auth.getProfile((err, profile) => {
      //   this.profile = profile;
      //   console.log(this.profile);
      // });
    }
  }

}
