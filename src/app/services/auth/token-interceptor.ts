import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Only send Bearer when you try to access private api
        if (request.url.includes('api/private')) {
            request = request.clone({
                headers: request.headers.set('Authorization', `Bearer ${this.authService.accessToken}`)
            });
        }
        return next.handle(request);
    }

}
