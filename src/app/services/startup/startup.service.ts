import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { StartUp } from 'src/app/models/start-up';
import { environment } from 'src/environments/environment';
import { Advisor } from 'src/app/models/advisor';
import { v4 as uuid } from "uuid";

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  startups$: Observable<StartUp[]> = null;

  constructor(private http: HttpClient) {}

  /**
   * GET Startup list
   */
  list(): Observable<StartUp[]> {
    return this.http.get<StartUp[]>(`${environment.apiUrl}/startups`);
  }

  /**
   * GET Startup by ID
   * @param id given startup id
   */
  get(id: string): Observable<StartUp> {
    return this.http.get<StartUp>(`${environment.apiUrl}/startups/${id}`);
  }

  /**
   * ADD Startup and return the new list
   * @param startup 
   */
  add(startup: StartUp): Observable<StartUp> {
    // We need to convert advisor to frontend advisor model because object assign does not...
    startup.advisor = new Advisor(startup.advisor._id, startup.advisor.lastName, startup.advisor.firstName, startup.advisor.description);
    return this.http.post<StartUp>(`${environment.apiUrl}/startups/`, startup);
  }

  /**
   * DELETE Startup by ID
   * @param id given startup id
   */
  delete(id: string) {
    return this.http.delete<StartUp>(`${environment.apiUrl}/startups/${id}`);
  }

  /**
   * UPDATE Startup and return the new list
   * @param startup 
   */
  update(startup: StartUp): Observable<StartUp> {
    return this.http.put<StartUp>(`${environment.apiUrl}/startups/${startup._id}`, startup);
  }

}
