import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Advisor } from 'src/app/models/advisor';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdvisorService {

  advisors$: Observable<Advisor[]> = null;

  constructor(private http: HttpClient) {}

  /**
   * GET Advisor list
   */
  list(): Observable<Advisor[]> {
    return this.http.get<Advisor[]>(`${environment.apiUrl}/advisors`);
  }

  /**
   * GET Advisor by ID
   * @param id given advisor id
   */
  get(id: string): Observable<Advisor> {
    return this.http.get<Advisor>(`${environment.apiUrl}/advisors/${id}`);
  }

    /**
   * ADD Startup and return the new list
   * @param startup 
   */
  add(advisor: Advisor): Observable<Advisor> {
    // We need to convert advisor to frontend advisor model because object assign does not...
    advisor = new Advisor(advisor._id, advisor.lastName, advisor.firstName, advisor.description);
    return this.http.post<Advisor>(`${environment.apiUrl}/advisors/`, advisor);
  }

  /**
   * DELETE Startup by ID
   * @param id given startup id
   */
  delete(id: string) {
    return this.http.delete<Advisor>(`${environment.apiUrl}/advisors/${id}`);
  }

  /**
   * 
   * @param advisor 
   */
  update(advisor: Advisor) {
    return this.http.put<Advisor>(`${environment.apiUrl}/advisors/${advisor._id}`, advisor);
  }
}
