import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { StartUp } from '../models/start-up';
import { Advisor } from '../models/advisor';
import { v4 as uuid } from "uuid";

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const advisors = [
      new Advisor(uuid(), "Ken", "Forster", "Head advisor"),
      new Advisor(uuid(), "Doe", "John", "Head advisor"),
      new Advisor(uuid(), "Bane", "Jane", "Head advisor"),
      new Advisor(uuid(), "Do", "Kali", "Head advisor")
    ]
    // Business line = filed#3 --> 10 letters
    // Legal representative = filed#4 --> 15 letters
    // Address = field#7 --> 25 letters
    const startups = [
      new StartUp(uuid(), "Sensewaves", "IOT", "Dr. Fivos Maniatakos", 2, "IoT sensor analytics optimised for energy data", "48 Rue René Clair 75018, Paris France", advisors[0]),
      new StartUp(uuid(), "Klaxoon", "Meeting", "Mr X", 4, "Manage your meetings", "", advisors[1]),
      new StartUp(uuid(), "Blacknut", "Video games", "Mr X", 3, "New cloud gamming platform", null, advisors[3]),
      new StartUp(uuid(), "Shadow", "Cloud computing", "Mr X", 1, "New cloud computing platform", "", advisors[2])
    ];
    return { startups, advisors };
  }

  genId<T extends StartUp | Advisor>(table: T[]): string {
    return uuid();
  }
}
