import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { StartupHandleComponent } from './startup/startup-handle/startup-handle.component';
import { AdvisorHandleComponent } from './advisor/advisor-handle/advisor-handle.component';
import { AuthGuard } from './services/guards/auth.guard';
import { CallbackComponent } from './callback/callback.component';
import { CanActivate } from '@angular/router/src/utils/preactivation';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "home",
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "callback",
    component: CallbackComponent
  },
  {
    path: "startups",
    component: StartupHandleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "advisors",
    component: AdvisorHandleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "404",
    component: NotFoundComponent
  },
  {
    path: "**",
    redirectTo: "/404"
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
